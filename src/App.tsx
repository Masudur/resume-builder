/** @format */

function App() {
	return (
		<>
			<h1 className='text-3xl font-bold underline text-cyan-500'>Hello resume builder.</h1>
		</>
	);
}

export default App;
